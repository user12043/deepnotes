#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 6.09.2020 - 16:42
# part of project deep
import keras

dataset = keras.preprocessing.image_dataset_from_directory("data/mnist", batch_size=64, image_size=(28, 28))
