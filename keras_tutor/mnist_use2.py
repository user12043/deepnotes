#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 8.09.2020 - 22:26
# part of project deep

import sys
import cv2
# import matplotlib.pyplot as plt
from keras import models

# (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
# input_data = test_images[1]
# plt.imshow(input_data)re
# plt.show()
# input_data = input_data.reshape(1, 28, 28, 1)
from keras_preprocessing.image import img_to_array

cam = cv2.VideoCapture((sys.argv[1] if not sys.argv[1].isdigit() else int(sys.argv[1])) if len(sys.argv) > 1 else 0)
cv2.namedWindow("original")
cv2.namedWindow("processed")

model = models.load_model("save/mnist.h5")

threshold = 0.6
cell_div = 8

while True:
    ret, frame = cam.read()
    if not ret:
        print("fail")
        break
    min_size = min(frame.shape[:2])
    cell_size = int(min_size / cell_div)
    output = frame.copy()
    for x in range(0, frame.shape[0], cell_size):
        for y in range(0, frame.shape[1], cell_size):
            cv2.rectangle(frame, (y, x), (y + cell_size, x + cell_size), (255, 0, 0))
    cv2.imshow("original", frame)
    frame = cv2.cvtColor(frame, cv2.COLOR_RGBA2GRAY)
    # frame = frame[:min_size][:min_size]  # Make it square
    # cv2.resize(frame, (280, 280))
    # frame = frame[250:, :150]
    # frame = cv2.resize(frame, (28, 28))
    #  only if writing black over white paper (plt.imshow should show purple background)
    frame = cv2.bitwise_not(frame)
    # cv2.imshow("processed", frame)
    k = cv2.waitKey(1)
    if k % 256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k % 256 == 32:
        # SPACE pressed

        detections = []

        for x in range(0, frame.shape[0], cell_size):
            for y in range(0, frame.shape[1], cell_size):
                rtl = frame[x:x + cell_size, y:y + cell_size]  # region to look
                rtl = cv2.resize(rtl, (28, 28))
                inp = img_to_array(rtl)
                inp = inp.reshape(1, 28, 28, 1)
                inp = inp.astype("float32")
                inp = inp / 255
                prediction = model.predict(inp)
                detections.append({"val": prediction.argmax(), "pr": prediction.max(), "pos": (y, x)})
                pass

        for detection in detections:
            # cv2.rectangle(output, detection["pos"], (detection["pos"][0] + cell_size, detection["pos"][1] + cell_size),
            #               (255, 0, 0))
            if detection["pr"] > threshold:
                # print("{}: {}".format(detection[0].argmax(), detection[0].max()))
                cv2.rectangle(output, detection["pos"],
                              (detection["pos"][0] + cell_size, detection["pos"][1] + cell_size),
                              (0, 255, 0))
                cv2.putText(output, "{}: {:.2f}".format(detection["val"], detection["pr"]),
                            (detection["pos"][0], detection["pos"][1] + cell_size),
                            cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 2)

        cv2.imshow("processed", output)
# he.predict(input_data)
# out = model.predict(input_data)
# print(out.argmax())
