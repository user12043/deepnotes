#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 9.01.2021 - 22:12
# part of project deep

import sys
import cv2
# import matplotlib.pyplot as plt
from keras import models

# (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
# input_data = test_images[1]
# plt.imshow(input_data)
# plt.show()
# input_data = input_data.reshape(1, 28, 28, 1)
from keras_preprocessing.image import img_to_array

cam = cv2.VideoCapture((sys.argv[1] if not sys.argv[1].isdigit() else int(sys.argv[1])) if len(sys.argv) > 1 else 0)
cv2.namedWindow("original", cv2.WINDOW_NORMAL)
cv2.namedWindow("processed", cv2.WINDOW_NORMAL)

model = models.load_model("save/mnist.h5")

threshold = 0.3
cell_size = 280

while True:
    ret, frame = cam.read()
    if not ret:
        print("fail")
        break
    frame = cv2.cvtColor(frame, cv2.COLOR_RGBA2GRAY)
    min_size = min(frame.shape)
    frame = frame[:min_size][:min_size]  # Make it square
    output = frame.copy()
    cv2.resize(frame, (280, 280))
    # cv2.imshow("original", frame)
    frame = cv2.resize(frame, (28, 28))
    #  only if writing black over white paper (plt.imshow should show purple background)
    frame = cv2.bitwise_not(frame)
    cv2.imshow("processed", frame)
    k = cv2.waitKey(1)
    if k % 256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    # elif k % 256 == 32:
    else:
        # SPACE pressed
        inp = img_to_array(frame)
        # plt.imshow(inp)
        # plt.show()
        inp = inp.reshape(1, 28, 28, 1)
        inp = inp.astype("float32")
        inp = inp / 255
        prediction = model.predict(inp)
        text = ""
        if prediction.max() > threshold:
            text = "{}: {}".format(prediction.argmax(), prediction.max())
        else:
            text = "-"
        cv2.putText(output, text, (5, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 2)
        cv2.imshow("original", output)
cv2.destroyAllWindows()
# he.predict(input_data)
# out = model.predict(input_data)
# print(out.argmax())
