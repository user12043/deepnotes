#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 10.03.2020 - 12:34
# part of project deep

# mnist dataset in conv ile yapılışı. Kitapta 5.1
# validation data da ekleniyor

from keras import layers
from keras import models
from keras.datasets import mnist
from keras.utils import to_categorical
from tensorflow.python.keras.callbacks import ModelCheckpoint

epoch = 20
batch_size = 64

model = models.Sequential()
# 32 tane 3x3 lük kernel kullanılacak, giriş resmi 28x28, tek kanallı (siyah beyaz)
# padding same yapak. resim zaten küçük diye dolgulama yapsın. (default değeri olan valid olunca dolgulama yapmıyor)
model.add(layers.Conv2D(32, (5, 5), (1, 1), activation='relu', input_shape=(28, 28, 1), padding="same"))
# model.add(layers.Dropout(0.25))
model.add(layers.MaxPooling2D((2, 2), (2, 2)))
model.add(layers.Conv2D(48, (5, 5), (2, 2), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
# model.add(layers.Conv2D(64, (3, 3), activation='relu'))

model.add(layers.Flatten())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

validation_images = train_images[-10000:]
train_images = train_images[:50000]
validation_labels = train_labels[-10000:]
train_labels = train_labels[:50000]

train_images = train_images.reshape((50000, 28, 28, 1))
train_images = train_images.astype('float32') / 255

validation_images = validation_images.reshape((10000, 28, 28, 1))
validation_images = validation_images.astype('float32') / 255

test_images = test_images.reshape((10000, 28, 28, 1))
test_images = test_images.astype('float32') / 255

train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)
validation_labels = to_categorical(validation_labels)

model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
#  set callback to save best weights
checkpoint = ModelCheckpoint("save/mnist_chk-{val_loss:.4f}-{val_accuracy:.4f}{epoch:02d}.h5", save_best_only=True)

h = model.fit(train_images, train_labels, epochs=epoch, batch_size=batch_size,
              validation_data=(validation_images, validation_labels), callbacks=checkpoint)

print("Evaluating test data")
test_loss, test_acc = model.evaluate(test_images, test_labels)

model.save("save/mnist-last")
