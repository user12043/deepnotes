#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 18.02.2020 - 12:38
# part of project deep

from keras import optimizers
from keras.datasets import mnist
from keras.layers import Dense
from keras.models import Sequential
from keras.utils import to_categorical

((train_images, train_labels),
 (test_images, test_labels)) = mnist.load_data()

train_images = train_images.reshape((60000, 28 * 28))
train_images = train_images.astype("float32") / 255
test_images = test_images.reshape((10000, 28 * 28))
test_images = test_images.astype("float32") / 255

network = Sequential(name="mnist")

network.add(Dense(256, activation="relu", input_shape=(28 * 28,)))
network.add(Dense(10, activation="softmax"))

# 1 katogorile (one hot encoding)
train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)
network.compile(optimizer=optimizers.RMSprop(), loss="categorical_crossentropy",
                metrics=["accuracy", "mean_squared_error"])

# 2 sparse kullan kendisi kategorilesin
# network.compile(optimizer=optimizers.RMSprop(), loss="sparse_categorical_crossentropy", metrics=["accuracy"])

network.summary()
network.fit(train_images, train_labels, epochs=10, batch_size=512)
