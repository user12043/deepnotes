#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
mnist uygulama 2
"""

from keras.datasets import mnist

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

train_images = train_images.reshape((60000, 28 * 28))
train_images = train_images.astype('float32')  # / 255
test_images = test_images.reshape((10000, 28 * 28))
test_images = test_images.astype('float32')  # / 255

from keras import models, layers

# from keras.layers import Dense
model = models.Sequential()
from keras import activations

model.add(layers.Dense(512,  # use_bias=True,
                       activation=activations.relu,  # activation='relu'
                       input_shape=(28 * 28,)))

model.add(layers.Dense(10,
                       activation='softmax', input_shape=(28 * 28,)))
model.summary()

from keras import optimizers

model.compile(optimizer=optimizers.RMSprop(lr=1e-4),  # 'rmsprop',
              loss='sparse_categorical_crossentropy',
              metrics=['acc', 'mse'])

# from keras.utils import to_categorical
# train_labels=to_categorical(train_labels)
h = model.fit(train_images,
              train_labels,
              batch_size=100,
              epochs=5)
model.save('mnist_m1.h5')

import numpy as np

# test
test_loss, test_acc, test_mse = model.evaluate(test_images, test_labels)
print('test_acc:', test_acc)
print('test_loss:', test_loss)
print('test_mse:', test_mse)

# ornek bir goruntu sec ve aga uygula
y = model.predict(test_images[0].reshape(1, 28 * 28))
i = np.argmax(y)
print('tahmin edilen= ', i)

# grafik islemleri
import matplotlib.pyplot as plt

plt.figure(1)
plt.plot(h.history['loss'])
plt.xlabel('epoch')
plt.ylabel('loss')
plt.figure(2)
plt.plot(h.history['acc'])
plt.xlabel('epoch')
plt.ylabel('accuracy')
plt.show()
