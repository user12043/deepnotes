#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
mnist uygulama
"""

from keras.datasets import mnist

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

train_images = train_images.reshape((60000, 28 * 28))
train_images = train_images.astype('float32') / 255
test_images = test_images.reshape((10000, 28 * 28))
test_images = test_images.astype('float32') / 255

from keras import models, layers

# from keras.layers import Dense
model = models.Sequential()
from keras import activations

model.add(layers.Dense(256,  # use_bias=True,
                       activation=activations.relu,  # activation='relu'
                       input_shape=(28 * 28,)))

model.add(layers.Dense(10,
                       activation='softmax'))
model.summary()

model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['acc', 'mse'])

from keras.utils import to_categorical

train_labels = to_categorical(train_labels)
model.fit(train_images,
          train_labels,
          batch_size=100,
          epochs=5)
