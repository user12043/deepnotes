#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 10.03.2020 - 12:34
# part of project deep

# cifar10 dataset in conv ile yapılışı. Kitapta 5.1

import numpy as np
from keras import layers
from keras import models
from keras.datasets import cifar10
from keras.utils import to_categorical

model = models.Sequential()
# 32 tane 3x3 lük kernel kullanılacak, giriş resmi 28x28, tek kanallı (siyah beyaz)
# padding same yapak. resim zaten küçük diye dolgulama yapsın. (default değeri olan valid olunca dolgulama yapmıyor)
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3), padding="same"))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))

model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()

train_images = train_images.reshape((50000, 32, 32, 3))
train_images = train_images.astype('float32') / 255

test_images = test_images.reshape((10000, 32, 32, 3))
test_images = test_images.astype('float32') / 255

train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
model.fit(train_images, train_labels, epochs=10, batch_size=64)

test_loss, test_acc = model.evaluate(test_images, test_labels)

print("Test loss: ", test_loss)
print("Test accuracy: ", test_acc)

y = model.predict(test_images[2].reshape(1, 32, 32, 3))
maximum = np.argmax(y)
print("max: ", maximum)
