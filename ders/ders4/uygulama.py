#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 25.02.2020 - 14:15
# part of project deep

import numpy as np
# 4. ders uygulama
from keras import layers, optimizers, losses, metrics
from keras.models import Sequential

epochs = 500
batch_size = 1

train_data = np.array([[0, 0, 0], [0, 1, 1], [1, 1, 0], [1, 0, 1]])
train_label = np.array([[0, 1], [0, 1], [1, 0], [1, 0]])

network = Sequential()
network.add(layers.Dense(3, activation="softmax", input_shape=(3,)))
network.add(layers.Dense(16, activation="relu"))
network.add(layers.Dense(2, activation="relu"))
network.compile(optimizer=optimizers.Adam(), loss=losses.binary_crossentropy, metrics=[metrics.binary_accuracy])

network.summary()

network.fit(train_data, train_label, epochs=epochs, batch_size=batch_size)
