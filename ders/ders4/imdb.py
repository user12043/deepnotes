#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 25.02.2020 - 12:16
# part of project deep

import numpy as np
from keras import layers, optimizers, losses, metrics
from keras.datasets import imdb
from keras.models import Sequential

num = 10000
epoch = 3
batch_size = 512


def vectorize_sequences(sequences, dimension=num):
    results = np.zeros((len(sequences), dimension))
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1
    return results


((train_data, train_labels),
 (test_data, test_labels)) = imdb.load_data(num_words=num)

x_train = vectorize_sequences(train_data)
x_test = vectorize_sequences(test_data)

y_train = np.asarray(train_labels).astype("float32")
y_test = np.asarray(test_labels).astype("float32")

network = Sequential()
network.add(layers.Dense(16, activation="relu", input_shape=(num,)))
network.add(layers.Dense(16, activation="relu"))
network.add(layers.Dense(1, activation="sigmoid"))
network.compile(optimizer=optimizers.RMSprop(lr=0.001), loss=losses.binary_crossentropy,
                metrics=[metrics.binary_accuracy])
network.summary()

# doğrudan eğitim
# network.fit(x_train, y_train, epochs=epoch, batch_size=batch_size)

# validation ile eğitim
x_validation = x_train[:5000]
partial_x_train = x_train[5000:]

y_validation = y_train[:5000]
partial_y_train = y_train[5000:]

history = network.fit(partial_x_train, partial_y_train, epochs=epoch, batch_size=batch_size,
                      validation_data=(x_validation, y_validation))

# TEST
network.evaluate(x_test, y_test)

# p.plot(history.history["binary_accuracy"])
# p.plot(history.history["val_binary_accuracy"])
# p.legend()
# p.show()
