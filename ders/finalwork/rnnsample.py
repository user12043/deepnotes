#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 1.06.2020 - 15:03
# part of project deep

import numpy as np
from keras import layers
from keras.models import Model

inputs = layers.Input(shape=(3, 1))

rnn = layers.SimpleRNN(1, activation="linear", return_sequences=True)(inputs)
#  ikinci katman da eklersen
# rnn2 = layers.SimpleRNN(1, activation="linear", return_sequences=True)(rnn)
model = Model(inputs=inputs, outputs=rnn)

# ağırlık, geri besleme ağırlık, bias
w = [np.array([[1.]]), np.array([[1.]]), np.array([[0.]])]
# bi katman daha olaydı aynısı yanına virgülden eklenirdi (ağırlık, geri ağırlık, bias, ağırlık, geri ağırlık, bias)
# w = [np.array([[1.]]), np.array([[1.]]), np.array([[0.]]), np.array([[1.]]), np.array([[1.]]), np.array([[0.]])]

input = np.array([1., 1., 1.]).reshape((1, 3, 1))
print("in: ", input)

output = model.predict(input)

print("out", output)
