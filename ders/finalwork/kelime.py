#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 1.06.2020 - 11:37
# part of project deep

# kelime vektörü token ilişkilendirme yöntemi ilk ikisi
#  one hot encoding ve word(token) embedding

# one hot her kelimeye bir index verir basitçe (sonra label indisin 1 olduğu diğerlerinin 0 olduğu vektör olur)

samples = ["me go merheba", "ben sınava girecem"]
token_index = {}  # boş dictionary

for sample in samples:
    for word in sample.split():
        if word not in token_index:
            token_index[word] = len(token_index) + 1

print(token_index)

# Embeddingde eise ağa bir embedding koyulur. Bu her kelimeye ait bir vektör çıkarır. Girişine ise kelime indisini alır
