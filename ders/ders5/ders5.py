#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 3.03.2020 - 12:09
# part of project deep

import keras
import matplotlib.pyplot as plt
from keras import models, layers
from keras.datasets import boston_housing

print(keras.__version__)

batch_size = 1
epochs = 10

(train_data, train_targets), (test_data, test_targets) = boston_housing.load_data()

mean = train_data.mean(axis=0)
train_data -= mean
std = train_data.std(axis=0)
train_data /= std

mean = test_data.mean(axis=0)
test_data -= mean
std = test_data.std(axis=0)
test_data /= std

network = models.Sequential()
network.add(layers.Dense(64, activation='relu',
                         input_shape=(train_data.shape[1],)))
network.add(layers.Dense(64, activation='relu'))
network.add(layers.Dense(1))
network.compile(optimizer='rmsprop', loss='mse', metrics=['mae'])
network.summary()

h = network.fit(train_data, train_targets, batch_size=batch_size, epochs=epochs)
plt.figure(0)
epoch = range(1, 1 + len(h.history["loss"]))
plt.plot(epoch, h.history["loss"])
plt.show()

plt.figure(1)
h_test = network.evaluate(test_data, test_targets)
