#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# created by user12043 on 3.03.2020 - 13:33
# part of project deep

# ezberlemenin önüne geçmek için (overfitting and underfitting)
# 1. Eğitim verisinin artırılması
# 2. Ağ karmaşıklığının (kapasitesinin, boyutunun) azaltılması
# 3. Weight regularization Ağırlıkların loss fonksiyonuna eklenmesi (L1, L2) ama sadece eğitim sırasında
# 4. Dropout eklenmesi. Bir katmandaki bazı ağırlıkları bilerek kaybetmek, sıfırlamak. Katmanla beraber tanımlanır. Her güncellemede o katmanın bazı ağırlıklarını sıfırlayıp kalanları da ikiyle çarpar.
# 5. varolan veriyi zenginleştirme teknikleri
